# Miiverse Worker

A Cloudflare Worker that hits the [Archiverse](https://archiverse.guide) API and serves Miiverse content on the Fediverse.

It's hosted at https://miipub.com

## Why?

Because it's fun and seemed doable, and I've been itching to do ActivityPub stuff in a Cloudflare Worker.

## How it works

All the data lives in an Archiverse server. When you make ActivityPub requests, it fetches data from the Archiverse server in the background (via the REST API) and returns ActivityPub content.

## Configuration

- `LOCAL_DOMAIN` - the ActivityPub domain (`https://miipub.com`)
- `BACKEND_URL` - URL to the Archiverse server (`https://archiverse.guide`)

## License

[WTFPL](http://www.wtfpl.net/txt/copying/)