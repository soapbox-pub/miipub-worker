import { Env, Post, User } from './types';
import { archiveUrl, fixYouTube } from './utils';

function renderWebfinger(env: Env, user: User) {
  const activityPubId = new URL(`/users/${user.name}`, env.LOCAL_DOMAIN).toString();
  const { host } = new URL(env.LOCAL_DOMAIN);

  return {
    subject: `acct:${user.name}@${host}`,
    aliases: [activityPubId],
    links: [{
      rel: 'self',
      type: 'application/activity+json',
      href: activityPubId,
    }, {
      rel: 'self',
      type: 'application/ld+json; profile="https://www.w3.org/ns/activitystreams"',
      href: activityPubId,
    }],
  };
}

function renderActor(env: Env, user: User) {
  const activityPubId = new URL(`/users/${user.name}`, env.LOCAL_DOMAIN).toString();

  const actor: any = {
    '@context': [
      'https://www.w3.org/ns/activitystreams',
      {
        manuallyApprovesFollowers: 'as:manuallyApprovesFollowers',
        toot: 'http://joinmastodon.org/ns#',
        discoverable: 'toot:discoverable',
      }
    ],
    id: activityPubId,
    type: 'Person',
    following: `${activityPubId}/following`,
    followers: `${activityPubId}/followers`,
    inbox: `${activityPubId}/inbox`,
    outbox: `${activityPubId}/outbox`,
    preferredUsername: user.name,
    name: user.screenName,
    summary: user.bio ? `<p>${user.bio}</p>` : '',
    url: activityPubId,
    manuallyApprovesFollowers: false,
    discoverable: true,
    endpoints: {
      sharedInbox: new URL('/inbox', env.LOCAL_DOMAIN).toString(),
    },
  };

  if (user.iconUri) {
    actor.icon = {
      type: 'Image',
      mediaType: 'image/png',
      url: archiveUrl(user.iconUri),
    };
  }

  if (user.sidebarCoverUrl) {
    actor.image = {
      type: 'Image',
      mediaType: 'image/png',
      url: archiveUrl(user.sidebarCoverUrl),
    };
  }

  return actor;
}

function renderNote(env: Env, post: Post) {
  const postId = new URL(`/posts/${post.id}`, env.LOCAL_DOMAIN).toString();
  const actorId = new URL(`/users/${post.name}`, env.LOCAL_DOMAIN).toString();

  const attachmentUri = post.imageUri || post.screenShotUri;
  const videoUrl = post.videoUrl ? fixYouTube(post.videoUrl) : undefined;

  const note: any = {
    '@context': [
      'https://www.w3.org/ns/activitystreams',
      {
        sensitive: 'as:sensitive',
      },
    ],
    id: postId,
    type: 'Note',
    inReplyTo: null,
    published: new Date(post.postedDate * 1000).toISOString(),
    url: postId,
    attributedTo: actorId,
    to: [
      'https://www.w3.org/ns/activitystreams#Public',
    ],
    cc: [
      new URL(`${actorId}/followers`, env.LOCAL_DOMAIN).toString(),
    ],
    sensitive: false,
    content: post.text ? `<p>${post.text}</p>` : '',
  };

  if (videoUrl) {
    note.content +=`<p><a href="${videoUrl}" target="_blank" rel="nofollow noopener noreferrer">${videoUrl}</a></p>`;
  }

  if (attachmentUri) {
    note.attachment = [
      {
        type: 'Document',
        mediaType: 'image/png',
        url: archiveUrl(attachmentUri),
      }
    ];
  }

  return note;
}

function renderCollection(_env: Env, id: string, totalItems: number) {
  return {
    '@context': 'https://www.w3.org/ns/activitystreams',
    id,
    type: 'OrderedCollection',
    totalItems,
  };
}

export {
  renderWebfinger,
  renderActor,
  renderNote,
  renderCollection,
}