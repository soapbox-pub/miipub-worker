import { Env, Post, User } from './types';

function fetchUser(env: Env, username: string): Promise<User> {
  return post<User>(env, '/api/users/getuser', { name: username });
}

function fetchPost(env: Env, postId: string): Promise<Post> {
  return post<Post>(env, '/api/posts/getpost', { id: postId });
}

function getUrl(env: Env, endpoint: string): string {
  const url = new URL(endpoint, env.BACKEND_URL);
  return url.toString();
}

async function post<T, P extends {} = any>(env: Env, endpoint: string, params: P): Promise<T> {
  const response = await fetch(getUrl(env, endpoint), {
    method: 'POST',
    body: JSON.stringify(params),
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    },
  });

  if (response.ok) {
    return response.json<T>();
  } else {
    throw response;
  }
}

export {
  fetchUser,
  fetchPost,
}