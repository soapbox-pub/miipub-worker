function archiveUrl(url: string): string {
  return 'https://web.archive.org/web/20171110012107if_/' + url;
}

function fixYouTube(url: string): string {
  const u = new URL(url);

  if (u.hostname.endsWith('youtube.com') && u.pathname.startsWith('/embed/')) {
    const videoId = u.pathname.split('/embed/')[1];
    return `https://www.youtube.com/watch?v=${videoId}`;
  } else {
    return url;
  }
}

export {
  archiveUrl,
  fixYouTube,
}