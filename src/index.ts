import { Router } from 'itty-router';
import { fetchPost, fetchUser } from './api';
import { Env } from './types';
import { renderActor, renderCollection, renderNote, renderWebfinger } from './views';

const router = Router();

router.get('/', () => {
  return new Response('Miipub worker is ready!');
});

router.get('/.well-known/webfinger', async ({ query }, env: Env) => {
  const acct = query?.resource.split('acct:')[1];
  const [username, domain] = (acct || '').split('@');

  if (username && domain === new URL(env.LOCAL_DOMAIN).host) {
    try {
      const user = await fetchUser(env, username);
      const view = renderWebfinger(env, user);
      const json = JSON.stringify(view);

      return new Response(json, {
        headers: {
          'Content-Type': 'application/json',
        },
      });
    } catch (e) {
      return new Response('', { status: 404 });
    }
  } else {
    return new Response('', { status: 400 });
  }
});

router.get('/users/:username', async ({ params }, env: Env) => {
  try {
    const user = await fetchUser(env, params!.username);
    const view = renderActor(env, user);
    const json = JSON.stringify(view);

    return new Response(json, {
      headers: {
        'Content-Type': 'application/activity+json',
      },
    });
  } catch (e) {
    return new Response('', { status: 404 });
  }
});

router.get('/users/:username/following', async ({ params }, env: Env) => {
  try {
    const user = await fetchUser(env, params!.username);
    const collectionId = new URL(`/users/${user.name}/following`, env.LOCAL_DOMAIN).toString();
    const view = renderCollection(env, collectionId, user.followingCount);
    const json = JSON.stringify(view);

    return new Response(json, {
      headers: {
        'Content-Type': 'application/activity+json',
      },
    });
  } catch (e) {
    return new Response('', { status: 404 });
  }
});

router.get('/users/:username/followers', async ({ params }, env: Env) => {
  try {
    const user = await fetchUser(env, params!.username);
    const collectionId = new URL(`/users/${user.name}/followers`, env.LOCAL_DOMAIN).toString();
    const view = renderCollection(env, collectionId, user.followerCount);
    const json = JSON.stringify(view);

    return new Response(json, {
      headers: {
        'Content-Type': 'application/activity+json',
      },
    });
  } catch (e) {
    return new Response('', { status: 404 });
  }
});

router.get('/posts/:postId', async ({ params }, env: Env) => {
  try {
    const post = await fetchPost(env, params!.postId);
    const view = renderNote(env, post);
    const json = JSON.stringify(view);

    return new Response(json, {
      headers: {
        'Content-Type': 'application/activity+json',
      },
    });
  } catch (e) {
    return new Response('', { status: 404 });
  }
});

async function inboxController() {
  return new Response('', { status: 200 });
};

router.post('/users/:username/inbox', inboxController);
router.post('/inbox', inboxController);

router.get('*', () => {
  return new Response('', { status: 404 });
});

export default {
  async fetch(
    request: Request,
    env: Env,
    ctx: ExecutionContext
  ): Promise<Response> {
    return router.handle(request, env, ctx);
  },
};
