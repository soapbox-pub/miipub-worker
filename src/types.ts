interface Env {
  LOCAL_DOMAIN: string
  BACKEND_URL: string
}

interface User {
  bio: string
  birthday: string
  country: string
  empathyCount: number
  favoriteGameGenres: string
  followerCount: number
  followingCount: number
  friendsCount: number
  gameSkill: number
  gameSystem: string
  iconUri: string
  id: number
  isBirthdayHidden: boolean
  isError: boolean
  isHidden: boolean
  name: string
  screenName: string
  sidebarCoverUrl: string
  totalDeletedPosts: number
  totalPosts: number
  totalReplies: number
}

interface Post {
  discussionType: unknown
  empathyCount: number
  feeling: number
  gameCommunityIconUri: string
  gameCommunityTitle: string
  gameId: string
  iconUri: string
  id: string
  imageUri: string
  isAcceptingResponse: boolean
  isPlayed: boolean
  isSpoiler: boolean
  name: string
  postedDate: number
  replyCount: number
  screenName: string
  screenShotUri: string
  text: string
  title: string
  titleId: string
  videoUrl: string
  warcLocation: string
}

export type {
  Env,
  User,
  Post,
}